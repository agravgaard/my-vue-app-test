# Serverless Website from Vue Starter Template

[Deployed website](https://d2qr9rd73vga1o.cloudfront.net/)

## Initialize
```
npm i -g serverless init vue-starter
```

## Project setup
```
npm install
```

## Deploy
```
serverless deploy
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Management of the deployment
[Serverless dashboard](https://app.serverless.com/agravgaard/apps/my-vue-app-test/vue-starter/dev)
